package main

import (
	"fmt"
)

func Solution(A []int) int {

	var res int
	if len(A) == 0 {
		panic("Пустой массив, я так не работаю!")
	}
	for i := 0; i < len(A); i++ {
		j := i
		for ; j > 0 && A[i] < A[j-1]; j-- {
		}
		for ; i > j; i-- {
			A[i], A[i-1] = A[i-1], A[i]
		}
	}
	fmt.Println(A)

	if A[0] <= 0 {
		panic("У вас числа отрицательные или ноль, я так не работаю!")
	}

	for i := 0; i < len(A); i++ {
		if A[i] != i+1 {
			res = i + 1
			break
		}
	}

	if res == 0 {
		panic("Нет пропущенного элемента")
	}
	return res
}

func main() {
	A := []int{10, 9, 6, 8, 11, 12, 7, 1, 2, 20, 18, 3, 4, 15, 5, 17, 13, 14, 16}

	B := []int{10, 9, 6, 8, 11, 12, 7, 1, 2, 19, 18, 3, 4, 15, 5, 17, 13, 14}

	//C := []int{10, 9, 6, 8, 11, 12, 7, 1, 2, 0, 18, 3, 4, 15, 5, 17, 13, 14}

	//	D := []int{}

	res := Solution(A)
	res1 := Solution(B)
	//res2 := Solution(C)
	//	res3 := Solution(D)

	fmt.Println(res, res1) //, res1, res3)
}
